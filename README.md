# nvim-config
## initial configuration for nvim editor

## Steps to configure 
~~~
sudo apt install neovim
cd ~/.config
git clone https://gitlab.com/kunaala/nvim-config.git
mv nvim-config nvim
~~~

## Make desired changes to the init.vim file 