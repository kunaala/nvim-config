set number
set nocompatible   "no need of backward compatability
filetype off       "to disable auto plugging of plugins
set hlsearch
set smartindent 
set termguicolors 

"to call plugin manager
call plug#begin('~/.config/nvim/plugged')
"Plug 'morhetz/gruvbox'
"Plug 'tpope/vim-fugitive'
Plug 'ayu-theme/ayu-vim'
Plug 'preservim/nerdtree'
Plug 'tomasiser/vim-code-dark'
Plug '907th/vim-auto-save', 
call plug#end()
"let ayucolor="light"
colorscheme codedark 

map <silent> <C-n>  :NERDTreeFocus<CR>
let g:auto_save = 1  " enable AutoSave on startup
" Set internal encoding of vim, not needed on neovim, since coc.nvim using some
" unicode characters in the file autoload/float.vim
set encoding=utf-8

" TextEdit might fail if hidden is not set.
set hidden

" Some servers have issues with backup files, see #649.
set nobackup
set nowritebackup

" Give more space for displaying messages.
set cmdheight=2

" Having longer updatetime (default is 4000 ms = 4 s) leads to noticeable
" delays and poor user experience.
set updatetime=300

" Don't pass messages to |ins-completion-menu|.
set shortmess+=c



